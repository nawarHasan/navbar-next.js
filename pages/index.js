import Link from "next/link";
import Head from "next/head";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title> Nawar Next Project</title>
      </Head>
      <Link href="/about"> About</Link>
      <h1 className={styles.homePageTitle}> Eessential</h1>
    </div>
  );
}
